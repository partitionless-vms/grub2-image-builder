#!/usr/bin/perl -w
use strict;
use warnings;
use Getopt::Long;
use File::Copy;
use File::Temp qw(tempdir tempfile);
use Cwd;


$|=1;
my $arch         = "i386-pc";
my $grub_lib     = "/usr/lib/grub";
my $grub_version = get_grub_version();
my $out_dir      = ".";
my $help;


GetOptions(
	"arch|a=s"   => \$arch,
	"grub-lib=s" => \$grub_lib,
	"output|o=s" => \$out_dir,
	"help|h"     => \$help
);

if ( $help ) {
	print <<HELP;
$0: Generate grub images for partitionless VMs

Syntax: $0 [ options ]

Options:
	-a, --arch        Architecture to build. i386-pc by default.
	--grub-lib        Grub's lib directory. /usr/lib/grub by default.
	-o, --output      Output directory
	-h, --help        This help
HELP
}


print "GRUB version: $grub_version\n";



my $mod_path = "$grub_lib/$arch/";
my $outfile = "$out_dir/grub2-${grub_version}-${arch}.img";



my $mods = generate_mod_list($arch);
my $memdisk = mk_memdisk();

mk_image($mods, $memdisk, $outfile);

symlink($outfile, "$out_dir/grub2-latest-${arch}.img");


sub get_grub_version {
	my $out = `grub2-mkimage --version`;
	chomp $out;
	$out =~ /(\d+\.\d+)$/;
	return $1;
}

sub mk_memdisk {
	my $tempdir = tempdir( CLEANUP => 1 );
	my (undef, $tempfile) = tempfile("/tmp/XXXXXXXXX", OPEN => 0, SUFFIX => ".tar");

	print "Generating memdisk...";
	mkdir("$tempdir/memdisk_boot");
	mkdir("$tempdir/memdisk_boot/grub");


	copy("config/$arch/grub.cfg", "$tempdir/memdisk_boot/grub/");
	my $curdir = getcwd;
	chdir($tempdir);
	system("tar", "-cvf", $tempfile, glob("*")) == 0
		or die "Failed to create tar file, return code $?";

	print " done\n";
	chdir($curdir);
	return $tempfile;
}

sub mk_image {
	my ($mods, $memdisk, $out) = @_;

	print "Generating $out...";
	system("grub2-mkimage",
		#"--prefix", "/boot/grub", 
		"--config", "config/$arch/grub_bootstrap.cfg",
		"--memdisk", $memdisk,
		"--compress=xz",
	       	"-o", $out, 
		"-O", $arch,
	       	keys %$mods ) == 0 or die "Failed to create image. Return code was $?";
	print " done\n";
}


sub generate_mod_list {
	my ($arch) = @_;

	print "Loading module list from config/$arch...\n";

	my %mods;
	for my $file ( glob("config/$arch/*.lst") ) {
		print "\tLoading $file... ";
		open(my $fh, '<', $file) or die "Can't open $file: $!";
		while(my $line = <$fh>) {
			chomp $line;
			$line =~ s/#.*$//; # remove comments

			my $mod;

			if ( $line =~ /:/ ) {
				(undef, $mod) = split(/:/, $line);
			} else {
				$mod = $line;
			}

			# Remove whitespace
			$mod =~ s/^\s+//;
			$mod =~ s/\s+$//;

			next unless ($mod); # Skip empty lines

			$mods{ $mod } = 1;
		}
		close $fh;
		print "done\n";
	}


	print "\t" . (scalar keys %mods) . " modules total\n";

	my $total_size = 0;
	my $str_mod_list = "";
	my %sizes;

	foreach my $mod ( keys %mods ) {
		my $mod_full_path = "$mod_path/$mod.mod";

		if ( -f "$mod_full_path" ) {
			my $size =  (stat("$mod_full_path"))[7];
			$total_size += $size;
			$sizes{$mod} = $size;
		} else {
			print "Can't find: $mod_full_path\n";
		}
	}



	print "\t$total_size bytes total\n";

	my @by_size = reverse sort { $sizes{$a} <=> $sizes{$b} } keys %mods;

	print "\t10 largest:\n";
	my $count=0;
	foreach my $m (@by_size) {
		print "\t\t$m:" . (" " x (25-length($m))) .  "$sizes{$m}\n";
		last if ($count++ >= 10);
	}


	return \%mods;
}



