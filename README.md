# grub2-image-builder

A partitionless setup requires an external grub, as grub2 can't be embedded into ext4. This creates a grub2 image that can be used as the "kernel" for KVM to boot.

This tool is a glorified shell script. It helps a bit by allowing separate configurations for multiple architectures, and showing the largest modules being built in.

The later part is necessary because i386-pc has size limits, and so every module can't be built in.
